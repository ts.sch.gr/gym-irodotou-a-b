/**************************************************************/
/*	         Drasthriothtes Controller                    */
/**************************************************************/
function goDrasthriothta(noumeraki)
{
drastMax=27;
drastMin=1;
drastURL=window.location.toString();
drastURL=drastURL.substr(drastURL.lastIndexOf("dr")+2);
drastURL=drastURL.substr(0,drastURL.indexOf("."));
drastCurrent=parseInt(drastURL);
if (noumeraki=="start") noumeraki=drastMin;
else if (noumeraki=="end") noumeraki=drastMax;
else noumeraki=drastCurrent+noumeraki;
if (noumeraki<drastMin) noumeraki=drastMax;
else if (noumeraki>drastMax) noumeraki=drastMin;
window.location.replace("dr"+noumeraki+".htm");
}




/**************************************************************/
/*			Question Type A (drag & drop) specific functions			*/
/**************************************************************/

function checkQuestionTypeA(strCorrectAnswers) {
	var i, iCorrectMatches, iMatches, arrCorrectAnswers, strAnswer;
	iCorrectMatches = 0;
	iMatches = 0;
	
	arrCorrectAnswers = strCorrectAnswers.split(',');
	
	strCorrectAnswers = ',' + strCorrectAnswers + ',';
	
	var i, colAnswerImages;
	
	colAnswerImages = document.all.item("AnswerImage");
	
	for (i=0; i < colAnswerImages.length; i++) {
		if (colAnswerImages.item(i).parentElement.tagName == 'DIV') {
			iMatches++;
			
			strAnswer = colAnswerImages.item(i).parentElement.id.substring(colAnswerImages.item(i).parentElement.id.indexOf('-')+1) + colAnswerImages.item(i).id.substring(colAnswerImages.item(i).id.indexOf('-')+1);
			if ( strCorrectAnswers.indexOf(',' + strAnswer + ',') >= 0 ) { iCorrectMatches++ }
		}
	}

	showResultsQuestionTypeA(iCorrectMatches, arrCorrectAnswers.length, strCorrectAnswers.substring(1, strCorrectAnswers.length-1));
}

function showResultsQuestionTypeA(lngCorrectAnswers, lngTotalAnswers, strCorrectAnswers) {
	var objDialogArgs = new Object();
	var lngRetVal;
	objDialogArgs.lngCorrectAnswers = lngCorrectAnswers;
	objDialogArgs.lngTotalAnswers = lngTotalAnswers;
	
	lngRetVal = window.showModalDialog("results.htm", objDialogArgs, 'dialogWidth:520px; dialogHeight:220px; scroll: no; status:no');
	if (lngRetVal == 1) {					// Retry
		document.location.reload();	
	} else if (lngRetVal == 2) {	// Show Solution
		showSolutionQuestionTypeA(strCorrectAnswers);
	}

/*	
	newWin = window.open('', 'winResults', 'width=500,height=30');
	if (newWin != null) {
		newDoc = newWin.document;
		newDoc.write('<html><head>');
		newDoc.write('<link rel="stylesheet" href="irodotos.css" type="text/css">')
		newDoc.write('</head><body class="drastiriotita">')
		if (lngCorrectAnswers == 0) {
				newDoc.write('��� ������� ������ ����� ��������.')
		} else if (lngCorrectAnswers == 1) {
				newDoc.write('������� <b>' + lngCorrectAnswers + '</b> ��������.')
		} else if (lngCorrectAnswers > 1) {
				newDoc.write('������� <b>' + lngCorrectAnswers + '</b> ����������.')
		}
		if (lngCorrectAnswers < lngTotalAnswers) {
			newDoc.write('<p align="center"><input type="button" class="submit" value="��������������" onclick="if (window.opener != null) {window.opener.document.location.reload(); window.close();}">&nbsp;&nbsp;&nbsp;<input type="button" class="submit" value="��� �� ����� ��������" onclick="if (window.opener != null) { window.opener.showSolutionQuestionTypeA(\'' + strCorrectAnswers + '\'); window.close();}"></p>');
		}
		newDoc.write('<p align="center"><input type="button" class="submit" value="��������" onclick="window.close()"></p>');		
		newDoc.write('</body></html>');
		newWin.focus();
	}
*/	
}

function showSolutionQuestionTypeA(strCorrectAnswers) {
	var i, iCorrectMatches, iMatches, arrCorrectAnswers, strAnswer;
	iCorrectMatches = 0;
	iMatches = 0;
	
	arrCorrectAnswers = strCorrectAnswers.split(',');
	
	var i, j, colAnswerImage;
	var strAnswerCode, strQuestionCode;
	var colAnswerPlaceHolder, colQuestionPlaceHolder;
	
	colAnswerImages = document.all.item("AnswerImage");

	for (i=0; i < colAnswerImages.length; i++) {
		strAnswerCode = '' + (i+1);
		strQuestionCode = '';
		
		for (j=0 ; j < arrCorrectAnswers.length ; j++) {
			if ( (arrCorrectAnswers[j].substring(1, arrCorrectAnswers[j].length) == strAnswerCode) ) { strQuestionCode = arrCorrectAnswers[j].substring(0,1) }
		}

		if (strQuestionCode.length > 0) {
			colQuestionPlaceHolder = document.all.item("Question-" + strQuestionCode);
		} else {
			colQuestionPlaceHolder = document.all.item("AnswerPlaceholder-" + strAnswerCode);
		}

		colAnswerImage = document.all.item("Answer-" + strAnswerCode);
		colQuestionPlaceHolder.appendChild(colAnswerImage);
	}
}


var objSourceImage, tmpDiv;

function DoDragStart(objElement) {
	objSourceImage = objElement;

	tmpDiv		=		document.getElementById('IMAGE_DRAG');
	
	tmpDiv.src = objElement.src;
	tmpDiv.style.posLeft	= window.event.clientX + document.body.scrollLeft;
	tmpDiv.style.posTop		= window.event.clientY + document.body.scrollTop;	
	tmpDiv.style.visibility = "visible";
	
	objSourceImage.style.visibility="hidden";
	
	return true;
}

function DoDrag(objElement) {
	tmpDiv.style.posLeft	= window.event.clientX + document.body.scrollLeft;
	tmpDiv.style.posTop		= window.event.clientY + document.body.scrollTop;	
	return true;
}

function DoDragEnd() {
	if (objSourceImage != null) {
		objSourceImage.style.visibility = "visible";
		document.getElementById('AnswerPlaceholder-' + objSourceImage.id.substring(objSourceImage.id.indexOf('-')+1)).appendChild(objSourceImage);		
	}

	objSourceImage = null;
	
	var tmpDiv; 
	tmpDiv		=	document.getElementById('IMAGE_DRAG');
	tmpDiv.src = "";
	tmpDiv.style.posLeft	= 0;
	tmpDiv.style.posTop		= 0;		
	tmpDiv.style.visibility = "hidden";

	return true;
}

function DoDragEnter(objElement) {
	var objElement;

	if (objSourceImage != null) {
		objElement.className = "DropAreaHighlight";
		
		window.event.returnValue = false;
		return false;		
	} else {
		window.event.returnValue = true;
		return true;
	}
}

function DoDragOver() {
	if (objSourceImage != null) {
		window.event.returnValue = false;
		return false;
	} else {
		window.event.returnValue = true;
		return true;
	}		
}

function DoDragLeave(objElement) {
	if (objSourceImage != null) {
		objElement.className = "DropArea";
		
		window.event.returnValue = false;
		return false;		
	} else {
		window.event.returnValue = true;
		return true;
	}
}

function DoDrop(objElement) {
	if (objSourceImage != null) {
		objElement.className = "DropArea";
		
		objElement.appendChild(objSourceImage);
		objSourceImage.style.visibility = "visible";

		objSourceImage = null;
		window.event.returnValue = false;
		return false;		
	} else {
		window.event.returnValue = true;
		return true;
	}
}



/**************************************************************/
/*		Question Type B (multiple choice) specific functions		*/
/**************************************************************/

function checkQuestionTypeB(strCorrectAnswers) {
	var i, j, iCorrectMatches, arrCorrectAnswer;
	iCorrectMatches = 0;
	
	arrCorrectAnswer = strCorrectAnswers.split(',');
	
	for (i=0 ; i < arrCorrectAnswer.length ; i++) {
		for (j=0 ; j < frmMain.answer.length ; j++) {
			if ( (arrCorrectAnswer[i] == frmMain.answer[j].value) && (frmMain.answer[j].checked) )	{ iCorrectMatches++; }
		}
	}

	showResultsQuestionTypeB(iCorrectMatches, arrCorrectAnswer.length, strCorrectAnswers);
}

function showResultsQuestionTypeB(lngCorrectAnswers, lngTotalAnswers, strCorrectAnswers) {
	var objDialogArgs = new Object();
	var lngRetVal;
	objDialogArgs.lngCorrectAnswers = lngCorrectAnswers;
	objDialogArgs.lngTotalAnswers = lngTotalAnswers;
	
	lngRetVal = window.showModalDialog("results.htm", objDialogArgs,'dialogWidth:520px; dialogHeight:220px; scroll: no; status:no');
	if (lngRetVal == 1) {					// Retry
		document.location.reload();	
	} else if (lngRetVal == 2) {	// Show Solution
		showSolutionQuestionTypeB(strCorrectAnswers);
	}
}

function showSolutionQuestionTypeB(strCorrectAnswers) {
	var i;
	
	strCorrectAnswers = ',' + strCorrectAnswers + ',';

	for (i=0 ; i < frmMain.answer.length ; i++) {
		if (strCorrectAnswers.indexOf(',' + frmMain.answer[i].value + ',') >= 0) {
			frmMain.answer[i].checked = true;
		} else {
			frmMain.answer[i].checked = false;
		}
	}
}



/**************************************************************/
/*		Question Type C (text field answer) specific functions	*/
/**************************************************************/

function checkQuestionTypeC(strCorrectAnswers) {
	var i, iCorrectMatches, arrCorrectAnswer, objPattern, strAnswer;
	iCorrectMatches = 0;
	
	arrCorrectAnswer = strCorrectAnswers.split(',');
	
	for (i=0 ; i < arrCorrectAnswer.length ; i++) {
		objPattern = eval("/" + arrCorrectAnswer[i] + "/i");
		
		try {	strAnswer = document.getElementById('answer-'+(i+1)).value } catch(error) {}
		strAnswer = strAnswer.replace(/�/i, '�').replace(/�/i, '�').replace(/�/i, '�').replace(/�/i, '�').replace(/�/i, '�').replace(/�/i, '�').replace(/�/i, '�');

		if ( objPattern.test(strAnswer) )	{ iCorrectMatches++; }
	}
	
	showResultsQuestionTypeC(iCorrectMatches, arrCorrectAnswer.length, strCorrectAnswers);
}

function showResultsQuestionTypeC(lngCorrectAnswers, lngTotalAnswers, strCorrectAnswers) {
	var objDialogArgs = new Object();
	var lngRetVal;
	objDialogArgs.lngCorrectAnswers = lngCorrectAnswers;
	objDialogArgs.lngTotalAnswers = lngTotalAnswers;
	
	lngRetVal = window.showModalDialog("results.htm", objDialogArgs,'dialogWidth:520px; dialogHeight:220px; scroll: no; status:no');
	if (lngRetVal == 1) {					// Retry
		document.location.reload();	
	} else if (lngRetVal == 2) {	// Show Solution
		showSolutionQuestionTypeC(strCorrectAnswers);
	}
}

function showSolutionQuestionTypeC(strCorrectAnswers) {
	var i;
	
	arrCorrectAnswer = strCorrectAnswers.split(',');
	
	for (i=0 ; i < arrCorrectAnswer.length ; i++) {
		try {	document.getElementById('answer-'+(i+1)).value = arrCorrectAnswer[i] } catch(error) {}
	}
}
